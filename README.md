# Readme #

This is an application to assist in the reading and singing of God's Word through metrical versions of the book of Psalms, or "Tehillim." 

Currently, it provides a simple interface for viewing the text of the Scottish Metrical Psalter of 1650. A working version can be found by downloading and building the master branch. It is being developed in MonoDevelop with GTK# and is dependent on [Json.NET](http://json.codeplex.com/) as well.

Feel free to contribute. Particularly, it would be helpful to have testing and instructions on running on other platforms. It is being developed on Debian GNU/Linux and should run on *nix systems with a recent version of Mono installed.

"O come, let us sing unto the Lord: let us make a joyful noise to the rock of our salvation.
Let us come before his presence with thanksgiving, and make a joyful noise unto him with psalms." (Psalm 95:1-2 AV)

## Screenshot ##

Git master branch from May 22, 2015; Includes multiple translation selection.

![teh2.png](https://bitbucket.org/repo/MRE975/images/2944408231-teh2.png)

Git master branch from May 1, 2015; First public version

![tehilim_point1.png](https://bitbucket.org/repo/MRE975/images/1798854757-tehilim_point1.png)



## Thanks to... ##
* Thanks to Romesh Prakashpalan for providing the 1650 Scottish Metrical Version source file in JSON format. It is also used at and was originally created for [χάρις](http://charisdevelopment.com/), an on-line Bible study tool.